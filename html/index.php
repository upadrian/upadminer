<?php
  function adminer_object() {
    include_once "plugins/plugin.php";
    foreach (glob("plugins/*.php") as $filename) {
      include_once $filename;
    }
    $plugins = [
      // specify enabled plugins here
      new AdminerJsonColumn,
      new AdminerDumpJson,
      new AdminerDumpZip,
      new AdminerEnumOption
    ];
    return new AdminerPlugin($plugins);
  }

  include "adminer-mysql-en.php";